import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import * as MapAPI from "./MapAPI";

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    if (!req.query.provider) {
        context.res = {
            status: 400,
            body: "Cannot process this request, since the provider is missing"
        }
        return;
    }

    if (!req.body) {
        context.res = {
            status: 400,
            body: "Cannot process this request, since the query is missing"
        }
        return;
    }

    let provider = req.query.provider;
    let query = req.body;

    if (provider === "HERE") {
        try {
            let result = await MapAPI.hereMapGeocodeAPI(query);
            context.res = {
                status: 200,
                body: result.toString()
            };
        }
        catch (err) {
            context.res = {
                status: 500,
                body: `${err}`
            };
        }
    }
    else if (provider === "Google") {
        try {
            let result = await MapAPI.googleMapGeocodeAPI(query);
            context.res = {
                status: 200,
                body: result.toString()
            };
        }
        catch (err) {
            context.res = {
                status: 500,
                body: `${err}`
            };
        }
    }
    else {
        context.res = {
            status: 400,
            body: "Cannot process this request, since the provider is not recognized"
        }
    }
};

export default httpTrigger;
