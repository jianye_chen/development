import fetch from "node-fetch";
import { URLSearchParams } from "url";

const heremap_key = 'UvF3XoVfqCuCSPIIMXkrvyQe3YGxTe3meuikX7pI4bk';
const googlemap_key = 'AIzaSyDL1disx6MXDHTa9NpHUoUYZSkC8Uo7abE';

export class MapAPIError extends Error {
    name = "MapAPIError";
    constructor(message: string) {
        super(message);
    }
}

export class GeographicCoordinate {
    lat: number;
    lng: number;

    /**
     * @param {number} lat Latitude
     * @param {number} lng Longitude
     * @throws {Error}
     */
    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;
    }

    toString(): string {
        return `${this.lat},${this.lng}`;
    }

    static fromStringCoordinate(coord: string): GeographicCoordinate {
        const origin = coord.split(',');
        return new GeographicCoordinate(
            parseFloat(origin[0].trim()),
            parseFloat(origin[1].trim())
        )
    }
};

export async function hereMapGeocodeAPI(query: {
    in?: string,
    q?: string,
    qq?: string
}): Promise<GeographicCoordinate> {
    const params = new URLSearchParams();
    params.append('apiKey', heremap_key);

    params.append('limit', "1");

    if (!query.q && !query.qq) {
        throw new MapAPIError(`HERE Geocode API: Must provide a query, either qualified or free-text.`);
    }

    if (query.q) {
        params.append('q', query.q);
    }

    if (query.qq) {
        params.append('qq', query.qq);
    }

    if (query.in) {
        params.append('in', query.in);
    }

    var request_string = 'https://geocode.search.hereapi.com/v1/geocode?' + params.toString();
    let response = await fetch(request_string);
    let data = await response.json();

    if (response.ok) {
        if (data['items'].length == 0) {
            let msg = "response is empty, address not recognized";
            throw new MapAPIError(`HERE API request '${request_string}' failed: ${msg}`);
        }
        let position = data['items'][0]['position'];
        return new GeographicCoordinate(position['lat'], position['lng']);
    }
    else {
        let msg = data['title'];
        throw new MapAPIError(`HERE API request '${request_string}' failed: ${msg}`);
    }
}

export async function googleMapGeocodeAPI(query: {
    address?: string,
    components?: string,
    region?: string
}): Promise<GeographicCoordinate> {
    const params = new URLSearchParams();
    params.append('key', googlemap_key);

    if (!query.address && !query.components) {
        throw new MapAPIError(`Google Geocode API: Must provide a query, either in address or in components`);
    }

    if (query.address) {
        params.append('address', query.address);
    }

    if (query.components) {
        params.append('components', query.components);
    }

    var request_string = 'https://maps.googleapis.com/maps/api/geocode/json?' + params.toString();
    let response = await fetch(request_string);
    let data = await response.json();

    if (response.ok && data["status"] === "OK") {
        const position = data["results"][0]["geometry"]["location"];
        return new GeographicCoordinate(position['lat'], position['lng']);
    }
    else {
        let msg = "null";
        if (response.ok) {
            msg = `Google Geocoder API request '${request_string}' failed: ${data["status"]}`;
        }
        else {
            msg = `Google Geocoder API request '${request_string}' failed with status code ${response.status}`;
        }
        throw new MapAPIError(msg);
    }
}
